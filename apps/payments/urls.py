# coding=utf-8
from django.conf.urls import url

from ..payments.views import stripe_checkout

urlpatterns = [
    url(r'^stripe/checkout/$', stripe_checkout, name='stripe_checkout'),
]
