import stripe
from django.contrib import messages
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from ..core.views import APIViewToken


class StripeCheckOut(APIViewToken):
    def post(self, request):
        amount = 1000

        # sk_test_6sFTW2ZKSnmoEdyVdxk3slOT
        # pk_test_SC6WWBdIE8vTt15vUPuDoGAb

        # Set your secret key: remember to change this to your live secret key in production
        # See your keys here https://dashboard.stripe.com/account/apikeys
        stripe.api_key = "sk_test_6sFTW2ZKSnmoEdyVdxk3slOT"

        # Get the credit card details submitted by the form
        token = request.POST['stripeToken']

        # Create the charge on Stripe's servers - this will charge the user's card
        try:
            charge = stripe.Charge.create(
                amount=amount,
                currency="usd",
                source=token,
                description="Example charge"
            )

            return Response("todo super")
        except stripe.error.CardError as e:
            messages.error(request, e.message)
            return Response(e.message)


stripe_checkout = StripeCheckOut.as_view()


@csrf_exempt
def stripe_post(request):
    if request.method == 'POST':

        amount = 1000

        # Set your secret key: remember to change this to your live secret key in production
        # See your keys here https://dashboard.stripe.com/account/apikeys
        stripe.api_key = "sk_test_gt1TKOpbY2y6PI1frR6xEiJt"

        # Get the credit card details submitted by the form
        token = request.POST['stripeToken']

        # Create the charge on Stripe's servers - this will charge the user's card
        try:
            charge = stripe.Charge.create(
                amount=amount,  # amount in cents, again
                currency="usd",
                source=token,
                description="Example charge"
            )

            messages.success(request, "super la transferencia")
            return redirect('stripe_create')
        except stripe.error.CardError as e:
            messages.error(request, e.message)
            return redirect('stripe_create')
