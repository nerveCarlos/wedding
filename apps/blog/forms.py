from datetime import datetime
from django import forms
from zinnia.admin.fields import MPTTModelMultipleChoiceField
from zinnia.admin.widgets import MPTTFilteredSelectMultiple, TagAutoComplete, MiniTextarea
from zinnia.models import Category
from zinnia.models import Entry
from django.utils.translation import ugettext_lazy as _

from ..blog.widgets import RelatedFieldWidgetWrapper


class EntryForm(forms.ModelForm):
    # categories = MPTTModelMultipleChoiceField(
    #     label=_('Categories'), required=False,
    #     queryset=Category.objects.all(),
    #     widget=MPTTFilteredSelectMultiple(_('categories')))

    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        # self.fields['sites'].widget = forms.HiddenInput()

        # self.fields['categories'].widget = RelatedFieldWidgetWrapper(
        #     self.fields['categories'].widget,
        #     Entry.categories.field.rel
        # )
        pass

    class Meta:
        model = Entry
        fields = ('title', 'status', 'content', 'sites')
        # widgets = {
        # 'tags': TagAutoComplete,
        # 'lead': MiniTextarea,
        # 'excerpt': MiniTextarea,
        # 'image_caption': MiniTextarea,
        # }
