import os

from datetime import datetime
from django.conf import settings
from django.contrib.sites.models import Site
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from zinnia.models import Entry

from ..core.views import GenericCreateView
from . import forms


class EntryCreateView(GenericCreateView):
    model = Entry
    form_class = forms.EntryForm
    success_url = 'zinnia:entry_archive_index'

    def get_initial(self):
        initial = super(EntryCreateView, self).get_initial()
        initial['sites'] = [Site.objects.first()]
        return initial

    def get_success_url(self):
        return super(EntryCreateView, self).get_success_url()

    def get_context_data(self, **kwargs):
        context = super(EntryCreateView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        form.instance.slug = datetime.now().strftime('%H%M%d%s')
        # form.instance.sites.add(Site.objects.first())
        return super(EntryCreateView, self).form_valid(form)


entry_create = EntryCreateView.as_view()


@csrf_exempt
def upload(request):
    if request.method == 'POST':
        f_name = handle_uploaded_file(request.FILES['file'])
        f_name = os.path.join(settings.MEDIA_URL, f_name)
        response = JsonResponse({'url': Site.objects.first().name + f_name})

        return response


def handle_uploaded_file(f):
    f_name = datetime.now().strftime('%H%M%d%s')
    with open(os.path.join(settings.MEDIA_ROOT, f_name), 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return f_name


def get_list(request, offset=0):
    files = os.listdir(settings.MEDIA_ROOT)
    offset = int(0 if not offset else 0)
    return render(request, "blog/images_template.html", {"imgs": files[offset:offset + 12], "total": len(files)})


def get_uploader_template(request):
    return render(request, "blog/modal_body.html")
