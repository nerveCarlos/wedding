# coding=utf-8
from django.conf.urls import url
from zinnia.views.entries import EntryDetail
from ..blog.views import entry_create, upload, get_uploader_template, get_list

urlpatterns = [
    url(r'^entry/add/$', entry_create, name='entry_create'),
    url(r'^entry/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', EntryDetail.as_view(),
        name='entry_detail'),
    url(r'^entry/upload/', upload, name='upload'),
    url(r"^entry/get_images/(?P<offset>\d+)?", get_list, name='get_image_list'),
    url(r'^entry/get_template/', get_uploader_template, name='get_uploader_template'),
]
