from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(is_safe=False)
def paginator(value, arg):
    size_page = int(arg)
    size = int(value)
    count = size / size_page

    if size % size_page != 0:
        count += 1

    result = '<ul class="pagination">'

    for i in range(count):
        result += """<li id="page_number%i" data-number="%s" class="%s"><a class="uploader_paginator" >%s</a></li>""" % (
        i, i, "" if i == 0 else "", i + 1)
    result += '</ul>'
    return mark_safe(result)


@register.filter()
def divide(value, arg):
    return int(value) / int(arg)
