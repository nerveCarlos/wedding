# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext
from django.template.loader import render_to_string

import six
from datatableview.helpers import keyed_helper
from datatableview.utils import get_field_definition, \
    DatatableStructure as ODatatableStructure, DatatableOptions


class DatatableStructure(ODatatableStructure):

    def __init__(self, ajax_url, options, model=None, column_filter=False):
        self.column_filter = column_filter
        super(DatatableStructure, self).__init__(ajax_url, options, model)

    def __unicode__(self):
        return render_to_string(self.options['structure_template'], {
            'url': self.url,
            'result_counter_id': self.options['result_counter_id'],
            'column_info': self.get_column_info(),
            'column_filter': self.column_filter
        })

    def get_column_attributes(self, name):
        attributes = super(DatatableStructure, self).get_column_attributes(name)
        index = self.options.get_column_index(name)

        if 'fixed_width' in self.options:
            if len(self.options['fixed_width']) > index >= 0:
                fixed_width = self.options['fixed_width'][index]
                if fixed_width:
                    attributes.update({'data-width': fixed_width})

        if 'row_class' in self.options:
            if len(self.options['row_class']) > index >= 0:
                row_class = self.options['row_class'][index]
                if row_class:
                    attributes.update({'data-rowclass': row_class})

        return attributes


def get_datatable_structure(ajax_url, options, model=None, column_filter=False):
    if not isinstance(options, DatatableOptions):
        options = DatatableOptions(model, {}, **options)

    return DatatableStructure(ajax_url, options, model=model, column_filter=column_filter)


@keyed_helper
def link_to_change(instance, text=None, *args, **kwargs):
    if not text:
        text = kwargs.get('default_value') or six.text_type(instance)
    return u"""<a href="{0}">{1}</a>""".format(instance.get_update_url(), text)


@keyed_helper
def make_yesno(value, *args, **kwargs):
    value = kwargs.get('default_value', value)
    if value:
        return '<span class="label label-success"><i class="fa fa-check"></i> %s</span>' % ugettext('si')
    return '<span class="label label-danger"><i class="fa fa-times"></i> %s</span>' % ugettext('no')
