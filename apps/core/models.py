# coding=utf-8
from __future__ import unicode_literals

from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.db.models.fields import FieldDoesNotExist
from django.forms.models import model_to_dict
from django.utils.translation import ugettext_lazy as _, ugettext
from django.core.urlresolvers import reverse, reverse_lazy, NoReverseMatch
# from django_permanent.models import PermanentModel
from model_utils.fields import StatusField, MonitorField
from model_utils.managers import QueryManager
from model_utils import Choices
from django_extensions.db.fields import AutoSlugField
import uuid


class BaseUUIDModel(models.Model):
    uuid = models.UUIDField(u'uuid', unique=True, db_index=True, editable=False, default=uuid.uuid4)

    class Meta:
        abstract = True

    def get_absolute_url(self):
        try:
            return reverse('%s_detail' % self._meta.model_name, kwargs={'pk': self.uuid})
        except NoReverseMatch:
            return ''

    def get_update_url(self):
        return reverse_lazy('%s_update' % self._meta.model_name, kwargs={'pk': self.uuid})

    def get_delete_url(self):
        return reverse_lazy('%s_delete' % self._meta.model_name, kwargs={'pk': self.uuid})

    def can_changed_by(self, user):
        if user:
            if user.has_perm('%s.change_%s' % (self._meta.app_label, self._meta.model_name)):
                return True
        return False

    def can_deleted_by(self, user):
        if user:
            if user.has_perm('%s.delete_%s' % (self._meta.app_label, self._meta.model_name)):
                return True
        return False


class BaseStatusModel(models.Model):
    STATUS = Choices(('active', ugettext('Activo')), ('inactive', ugettext('Inactivo')))
    status = StatusField(_('estado'), default=STATUS.active)
    status_changed = MonitorField(_('status changed'), monitor='status', editable=False)

    class Meta:
        abstract = True

    def is_active(self):
        return self.status == self.STATUS.active


# class BaseNomenclator(PermanentModel, BaseUUIDModel):
class BaseNomenclator(BaseUUIDModel):
    hidden = models.BooleanField(default=False, editable=False)

    class Meta:
        abstract = True

    def is_active(self):
        return not self.hidden


class BaseStatusNomenclator(BaseNomenclator, BaseStatusModel):

    class Meta:
        abstract = True

    def is_active(self):
        return self.status == self.STATUS.active and not self.hidden


class BaseNamedNomenclator(BaseStatusNomenclator):
    name = models.CharField(_(u'nombre'), max_length=200,
                            help_text=_('Nombre con que se identificará en el sistema'))

    class Meta:
        abstract = True
        ordering = ('name',)

    def __unicode__(self):
        return self.name

    def verbose_name(self):
        return unicode(self.__unicode__())


class BaseSlugNomenclator(BaseNamedNomenclator):
    slug = AutoSlugField(max_length=255, populate_from='name', unique=True, db_index=True,
                         blank=False, editable=False)

    class Meta:
        abstract = True

    def get_absolute_url(self):
        try:
            return reverse('%s_detail' % self._meta.model_name, kwargs={'slug': self.slug})
        except NoReverseMatch:
            return ''

    def get_update_url(self):
        return reverse_lazy('%s_update' % self._meta.model_name, kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse_lazy('%s_delete' % self._meta.model_name, kwargs={'slug': self.slug})


class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in
                             self._meta.fields])


def add_status_query_managers(sender, **kwargs):
    """
    Add a Querymanager for each status item dynamically.

    """
    if not issubclass(sender, BaseStatusModel):
        return
    for value, display in getattr(sender, 'STATUS', ()):
        try:
            sender._meta.get_field(value)
            raise ImproperlyConfigured("BaseStatusModel: Model '%s' has a field "
                                       "named '%s' which conflicts with a "
                                       "status of the same name."
                                       % (sender.__name__, value))
        except FieldDoesNotExist:
            pass
        sender.add_to_class(value, QueryManager(status=value))

models.signals.class_prepared.connect(add_status_query_managers)
