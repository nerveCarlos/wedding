# coding=utf-8
from __future__ import unicode_literals
import json
from django.core.exceptions import ObjectDoesNotExist, FieldError, ValidationError, PermissionDenied
from django.core.urlresolvers import reverse_lazy, reverse
from django.db import models
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import TemplateView, View
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils.timezone import datetime
from django.views.generic.detail import SingleObjectMixin, DetailView
from braces import views as braces
from datatableview.views import DatatableView as SkelDatatableView
from django.views.generic.edit import CreateView, UpdateView
import operator
from datatableview.utils import get_field_definition, split_real_fields, resolve_orm_path
from django.db.models import Q
from oauth2_provider.ext.rest_framework import permissions
from rest_framework import generics
from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView

from .datatables import get_datatable_structure

try:
    from functools import reduce
except ImportError:
    pass


class PermissionRequiredMixin(braces.PermissionRequiredMixin):
    def check_permissions(self, request):
        return True

    def dispatch(self, request, *args, **kwargs):
        has_permission = self.check_permissions(request)
        has_permission = True

        if not has_permission:  # If the user lacks the permission
            if request.user.is_authenticated():
                if self.raise_exception:
                    raise PermissionDenied  # Return a 403
                else:
                    if 'HTTP_REFERER' in request.META and reverse('account_login') not in request.META['HTTP_REFERER']:
                        messages.error(request,
                                       _('No tiene los permisos suficientes para entrar en la página solicitada.'))
                        return redirect(request.META['HTTP_REFERER'])
                    else:
                        messages.error(request,
                                       _('Ha sido redireccionado a su página de inicio porque no tiene los permisos '
                                         'suficientes para entrar en la página solicitada.'))

                        return redirect('home')
            return self.no_permissions_fail(request)

        return super(PermissionRequiredMixin, self).dispatch(request, *args, **kwargs)


# class HomePageView(braces.LoginRequiredMixin, TemplateView):
class HomePageView(TemplateView):
    template_name = 'core/home.html'


home = HomePageView.as_view()


class UUIDObjectMixin(SingleObjectMixin):
    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        pk = self.kwargs.get(self.pk_url_kwarg, None)
        if pk is not None:
            try:
                queryset = queryset.filter(uuid=pk)
            except FieldError:
                return super(UUIDObjectMixin, self).get_object(queryset)
        else:
            return super(UUIDObjectMixin, self).get_object(queryset)

        try:
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj


class GenericMixin(object):
    template_base = None

    def get_model(self):
        if self.model:
            return self.model
        else:
            return self.queryset.model

    def get_meta(self):
        if self.model:
            return self.model._meta
        else:
            return self.queryset.model._meta

    def get_template_base(self):
        if self.template_base:
            template_base = self.template_base
        else:
            m = self.get_model()._meta
            template_base = '%s/%s_base.html' % (m.app_label, m.model_name)

        return template_base

    def get_context_data(self, **kwargs):
        context = super(GenericMixin, self).get_context_data(**kwargs)
        context['opts'] = self.get_meta()
        context['template_base'] = self.get_template_base()
        return context


class BaseDatatableView(PermissionRequiredMixin, SkelDatatableView):
    def get_permission_required(self, request=None):
        if self.permission_required is None:
            m = self.get_model()._meta
            return '%s.view_%s' % (m.app_label, m.model_name)

        return self.permission_required

    def get_datatable(self):
        options = self._get_datatable_options()

        return get_datatable_structure(self.request.path, options, model=self.model,
                                       column_filter=self.has_filter_options())

    def get_context_data(self, **kwargs):
        context = super(BaseDatatableView, self).get_context_data(**kwargs)
        context['filter_options'] = self.get_filter_options()
        return context

    def has_filter_options(self):
        if hasattr(self, 'filter_options'):
            return True
        else:
            return False

    def get_filter_options(self):
        if hasattr(self, 'filter_options'):
            return json.dumps(self.filter_options)
        else:
            return None

    def apply_queryset_options(self, queryset):
        queryset = self.column_search(queryset)
        return super(BaseDatatableView, self).apply_queryset_options(queryset)

    def column_search(self, queryset):
        dt_data = self.request.GET
        if 'sSearch' in dt_data:
            for idx in xrange(int(dt_data['iColumns'])):
                search = dt_data['sSearch_%s' % idx]
                if search and search != dt_data['sRangeSeparator']:
                    if hasattr(self, 'search_col_%s' % idx):
                        custom_search = getattr(self, 'search_col_%s' % idx)
                        queryset = custom_search(search, queryset)
                    else:
                        field_tuple = self.datatable_options['columns'][idx]
                        all_fields = get_field_definition(field_tuple)[1]
                        fields, vt_fields = split_real_fields(self.model, all_fields)

                        if fields:
                            # filtro por rango
                            if dt_data['sRangeSeparator'] in search:
                                min, max = search.split(dt_data['sRangeSeparator'])

                                if min.isdigit():
                                    pass
                                else:
                                    try:
                                        min = datetime.strptime(min, '%m/%d/%Y')
                                    except ValueError:
                                        min = None

                                if min:
                                    criterions_min = [(Q(**{'%s__gte' % field: min})) for field in fields]
                                    search_min = reduce(operator.or_, criterions_min)
                                    try:
                                        queryset = queryset.filter(search_min)
                                    except (TypeError, ValidationError,):
                                        pass

                                if max.isdigit():
                                    pass
                                else:
                                    try:
                                        max = datetime.strptime('%s 23:59:59:999999' % max, '%m/%d/%Y %H:%M:%S:%f')
                                    except ValueError:
                                        max = None

                                if max:
                                    criterions_max = [(Q(**{'%s__lte' % field: max})) for field in fields]
                                    search_max = reduce(operator.or_, criterions_max)
                                    try:
                                        queryset = queryset.filter(search_max)
                                    except (TypeError, ValidationError,):
                                        pass

                            else:
                                field = resolve_orm_path(self.model, fields[0])
                                if isinstance(field, models.BooleanField):
                                    if search.lower() in ('true', 'yes', 'si'):
                                        search = True
                                    elif search.lower() in ('false', 'no'):
                                        search = False
                                    else:
                                        continue

                                    criterions = [Q(**{'%s' % field: search}) for field in fields]
                                    if len(criterions) > 0:
                                        search = reduce(operator.or_, criterions)
                                        queryset = queryset.filter(search)

                                elif dt_data['bRegex_%s' % idx] == 'true':
                                    criterions = [Q(**{'%s__iregex' % field: search}) for field in fields]
                                    if len(criterions) > 0:
                                        search = reduce(operator.or_, criterions)
                                        queryset = queryset.filter(search)
                                else:
                                    for term in search.split():
                                        criterions = (Q(**{'%s__icontains' % field: term}) for field in fields)
                                        search = reduce(operator.or_, criterions)
                                        queryset = queryset.filter(search)
        return queryset


class GenericDatatableView(GenericMixin, BaseDatatableView):
    create_url = None

    def get_create_url(self):
        if self.create_url:
            url = self.create_url
        else:
            m = self.get_meta()
            url = '%s_create' % m.model_name

        return reverse_lazy(url)

    def get_context_data(self, **kwargs):
        context = super(GenericDatatableView, self).get_context_data(**kwargs)
        context['has_add_permission'] = self.has_add_permission()
        context['create_url'] = self.get_create_url()
        return context

    def has_add_permission(self):
        m = self.get_meta()
        add_perm_key = '%s.add_%s' % (m.app_label, m.model_name)
        return self.request.user.has_perm(add_perm_key)


class GenericDetailView(GenericMixin, UUIDObjectMixin, PermissionRequiredMixin, DetailView):
    def get_permission_required(self, request=None):
        if self.permission_required is None:
            m = self.get_meta()
            return '%s.view_%s' % (m.app_label, m.model_name)

        return self.permission_required


class GenericEditMixin(PermissionRequiredMixin, braces.FormValidMessageMixin, GenericMixin):
    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            m = self.get_meta()
            url = '%s_list' % m.model_name

        return reverse_lazy(url)


class GenericCreateView(GenericEditMixin, CreateView):
    def get_permission_required(self, request=None):
        if self.permission_required is None:
            m = self.get_meta()
            return '%s.add_%s' % (m.app_label, m.model_name)

        return self.permission_required

    def get_success_url(self):
        # m = self.get_meta()
        # view_permission = '%s.view_%s' % (m.app_label, m.model_name)
        # if not self.request.user.has_perm(view_permission):
        #     return reverse_lazy('%s_create' % m.model_name)
        return super(GenericCreateView, self).get_success_url()

    def get_form_valid_message(self):
        m = self.get_meta()
        return ugettext("{model_name} {object_name} ha sido creado.".format(
            model_name=m.verbose_name, object_name=self.object))


class GenericUpdateView(GenericEditMixin, UUIDObjectMixin, UpdateView):
    def get_permission_required(self, request=None):
        if self.permission_required is None:
            m = self.get_meta()
            return '%s.change_%s' % (m.app_label, m.model_name)

        return self.permission_required

    def get_form_valid_message(self):
        if hasattr(self, 'verbose_name'):
            return _("Los cambios a %s han sido guardados." % self.object.verbose_name())
        return ugettext("Los cambios a %s han sido guardados." % self.object)


class GenericDeleteView(PermissionRequiredMixin, GenericMixin, UUIDObjectMixin, View):
    success_message = None
    success_url = None

    def get_permission_required(self, request=None):
        if self.permission_required is None:
            m = self.get_meta()
            return '%s.delete_%s' % (m.app_label, m.model_name)

        return self.permission_required

    def get_success_message(self):
        try:
            return self.success_message % self.object if self.success_message else ugettext(
                '%s ha sido eliminado.' % self.object)
        except TypeError:
            return self.success_message if self.success_message else ugettext('El item ha sido eliminado.')

    def get_success_url(self):
        if self.success_url:
            url = self.success_url
        else:
            m = self.get_model()._meta
            url = '%s_list' % m.model_name

        return reverse_lazy(url)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.mk_delete():
            messages.success(request, self.get_success_message())

        url = self.request.META.get('HTTP_REFERER', None) or self.get_success_url()
        return HttpResponseRedirect(url)

    def mk_delete(self):
        if self.object.mk_delete(user=self.request.user):
            return True
        return False


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class GenericsListCreateAPIViewToken(generics.ListCreateAPIView):
    permission_classes = [permissions.TokenHasScope]
    required_scopes = ['read', 'write']


class APIViewToken(APIView):
    permission_classes = [permissions.TokenHasScope]
    required_scopes = ['read', 'write']
