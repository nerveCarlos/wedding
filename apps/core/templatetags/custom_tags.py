﻿# coding=utf-8
from __future__ import unicode_literals

import re

from allauth.socialaccount import providers
from django.core.urlresolvers import reverse
import markdown
from django.template.defaultfilters import stringfilter
from django.utils.encoding import force_unicode
from django import template
from HTMLParser import HTMLParser
import json
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

register = template.Library()


@register.filter
@stringfilter
def currency(value):
    """
    Convierte un decimal a string con formato de dinero
    """
    if value != 'None':
        orig = force_unicode(value)
        new = re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', orig)
        if orig == new:
            if len(new) > 2:
                if new[-2] == '.':
                    new += '0'
            return '$ %s' % new
        else:
            return currency(new)
    return None


currency.is_safe = True


@register.filter(name='can_change')
def can_change(value, arg):
    """determina si el usuario puede modificar esta instancia"""
    return arg.can_changed_by(value)


@register.filter(name='can_delete')
def can_delete(value, arg):
    """determina si el usuario puede eliminar esta instancia"""
    return arg.can_deleted_by(value)


class MarkDownNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        return markdown_tag.markdown(self.nodelist.render(context))


@register.tag('markdown')
def markdown_tag(parser, token):
    """
    Enables a block of markdown text to be used in a template.

    Syntax::

            {% markdown %}
            ## Markdown

            Now you can write markdown in your templates. This is good because:

            * markdown is awesome
            * markdown is less verbose than writing html by hand

            {% endmarkdown %}
    """
    nodelist = parser.parse(('endmarkdown',))
    # need to do this otherwise we get big fail
    parser.delete_first_token()
    return MarkDownNode(nodelist)


@register.filter
def home_url():
    return reverse('home')


@register.filter(name='add_values')
def add_values(num1, num2):
    """Return the value of the addition of two numbers"""
    return num1 + num2


@register.filter(name='res_values')
def res_values(num1, num2):
    """Return the value of the rest of two numbers"""
    return num1 - num2


@register.filter(name='zip')
def zip_lists(a, b):
    return zip(a, b)


@register.filter(name='any')
def any_condition(l, condition):
    func = eval(condition)
    for item in l:
        if func(item):
            return True

    return False


@register.filter("any_bool")
def any_boolean(l):
    return any(l)


@register.filter(name='star_string')
@stringfilter
def star_string(string, tamano):
    """Return the begin of string with size "tamano".

    Example usage: {{ value|tamano:3 }}
    """
    return string[:tamano]


class AngularizeParser(HTMLParser):
    content = ""
    extra_attrs = {}

    def stringigy_attrs(self, attrs):
        result = ""
        for k, v in attrs.items():
            result += "%s='%s'" % (k, v)

        return result

    def to_camel_case(self, name):
        name = name.split('-')

        result = name[0]
        for n in name[1:]:
            result += n.capitalize()
        return result

    def dictionary_attrs(self, attrs):
        attrs_dict = {}
        for k, v in attrs:
            attrs_dict[k] = v
        return attrs_dict

    def put_errors(self, extra):
        extra = extra.split()

        errors_dict = {
            "password": _("La contraseña debe contener al menos 4 caracteres"),
            "same-field": _("Las contraseñas deben coincidir"),
            "required": _("Este campo es requerido"),
            "email": _("No es una dirección de email válida"),
            "custom-email": _("No es una dirección de email válida"),
            "exists-user": _("El nombre de usuario ya existe"),
            "exists-phone": _("Este número de teléfono ya está registrado"),
            "exists-email": _("Este email ya está registrado"),
            "same-email": _("Las direcciones de correo deben coicidir"),
            "usa-mask": _("No es un número de teléfono válido de Estados Unidos o de Cuba")

        }

        result = ""

        for directive in extra:
            if directive in errors_dict:
                result += """<div ng-message="%s">%s</div>""" % (self.to_camel_case(directive), errors_dict[directive])

        return result

    def handle_starttag(self, tag, attrs):
        attrs_dict = self.dictionary_attrs(attrs)
        extra = ''
        errors = ''

        if "name" in attrs_dict and tag == "input":
            attrs_dict["ng-model"] = attrs_dict["name"]
            if "value" in attrs_dict:
                attrs_dict["ng-init"] = "%s =\"%s\"" % (attrs_dict["name"], attrs_dict["value"])
            if attrs_dict["name"] in self.extra_attrs:
                extra += self.extra_attrs[attrs_dict["name"]]
                errors += """<div class='errors' ng-messages="form.%s.$error" ng-show='form.%s.$dirty'>
                                %s
                             </div>""" % (attrs_dict["name"], attrs_dict["name"], self.put_errors(extra))

        self.content += "<%s %s %s>%s" % (tag, self.stringigy_attrs(attrs_dict), extra, errors)

        # adding form error

    def handle_endtag(self, tag):
        self.content += "</%s>" % tag

    def handle_data(self, data):
        self.content += data


@register.simple_tag()
def angularize(value, *args, **kwargs):
    parser = AngularizeParser()
    parser.extra_attrs = kwargs
    parser.feed(value)

    return mark_safe(parser.content)


@register.assignment_tag
def get_facebook_provider():
    """
    Usage: `{% get_facebook_provider as facebook_provider %}`.
    """
    a = providers.registry.get_list()

    if a[0].id == "facebook":
        return a[0]
    elif a[1].id == "facebook":
        return a[1]


@register.assignment_tag
def get_google_provider():
    """
    Usage: `{% get_google_provider as google_provider %}`.
    """
    a = providers.registry.get_list()

    if a[0].id == "google":
        return a[0]
    elif a[1].id == "google":
        return a[1]
