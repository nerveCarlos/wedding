/**
 * Created by javier on 3/31/16.
 */


(function($) {

    $.fn.uploader = function(options) {
        var self  = this;

        var $form = $(this);
        var input = $(this).find("#file_input");
        var errorMsg = $(this).find("#error-message");
        var baseInput = document.getElementById("file_input");
        baseInput.addEventListener('change', display, false);

        self.clean = function(){
            $form[0].reset();
            $($form).find("#preview").removeAttr("src");
        };
        self.reload = function(){
             $(self).find("#thumbnail-container").load(
            options.imageLoadUrl, null, function(){
                $(self).find("img.selectable-image").click(function(){
                    var src = $(this).attr("absolute-uri");
                    options.callback(
                        {url: src}
                    );
                });
            }
        );
        };

        var isAdvancedUpload = function() {
                  var div = document.createElement('div');
                  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();

        function show_enabled(current) {

            load_page(current);
        }

        function load_page(number){

            $(self).find("#thumbnail-container").load(
            options.imageLoadUrl + number*size, null, function(){
                $(self).find("img.selectable-image").click(function(){
                     var src = $(this).attr("absolute-uri");
                    console.log(src);
                    options.callback(
                        {url: src}
                    );
                });

                $(self).find(".uploader_paginator").click(
                    function(){
                        show_enabled(parseInt($(this).parent().attr("data-number")));
                    }
                );

                var bellow = 4;
                var lis = $("li[data-number]");

                lis.hide();

                for(var i = 0; i < bellow; i++)
                    $("li[data-number="+ (number - i) +"]").show();

                for(i = 0; i < bellow; i++)
                    $("li[data-number="+ (number + i) +"]").show();
                $(lis[lis.length - 1]).show();

                $("li[data-number=" + number +"]").addClass("active");

            }
        );
        }

        var offset = 0;
        var size = 12;

        show_enabled(0);

        function display(e) {
            var reader = new FileReader();
            reader.onload = function (event) {
                console.log("previewing");
                $(self).find('.preview').attr('src', event.target.result);
            };
            reader.readAsDataURL(e.target.files[0]);
        }



        if (isAdvancedUpload) {
            $form.addClass('has-advanced-upload');
        }

        if (isAdvancedUpload) {

            var droppedFiles = false;

            $form.on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                e.preventDefault();
                e.stopPropagation();
            })
                .on('dragover dragenter', function () {
                    $form.find(".box__input").addClass('is-dragover');
                })
                .on('dragleave dragend drop', function () {
                    $form.find(".box__input").removeClass('is-dragover');
                })
                .on('drop', function (e) {
                    droppedFiles = e.originalEvent.dataTransfer.files;

                    var img = $($form).find("#preview");

                    var reader = new FileReader();
                    reader.onload = function (e) {

                        // e.target.result holds the DataURL which
                        // can be used as a source of the image:

                        img.attr('src', e.target.result);
                    };

                    reader.readAsDataURL(droppedFiles[0]);

                });
        }
        $form.on('submit', function (e) {
            if ($form.hasClass('is-uploading')) return false;

            $form.addClass('is-uploading').removeClass('is-error');

            if (isAdvancedUpload) {
                e.preventDefault();
                var ajaxData = new FormData($form.get(0));

                if (droppedFiles) {
                    ajaxData.append(input.attr('name'), droppedFiles[0]);
                }

                $.ajax({
                    url: options.url,
                    type: $form.attr('method'),
                    data: ajaxData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    complete: function () {
                        $form.removeClass('is-uploading');
                    },
                    success: function (data) {
                        options.callback(data);

                        $form[0].reset();
                        $($form).find("#preview").removeAttr("src");

                        $form.addClass(data.success == true ? 'is-success' : 'is-error');
                        if (!data.success) errorMsg.text(data.error);

                    },
                    error: function () {
                        // Log the error, show an alert, whatever works for you
                    }
                });
            } else {
                // ajax for legacy browsers
            }
        });

        return this;
    };

}( jQuery ));