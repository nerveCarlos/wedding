from rest_framework import serializers

from ..vendors.models import Venue, VenueType, Reviews, Rooms, Place, Catering, Photographer, Florist, Cake, Stationery, \
    Beauty


class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venue


class VenueTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VenueType


class ReviewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reviews


class RoomsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rooms


class CateringSerializer(serializers.ModelSerializer):
    class Meta:
        model = Catering


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place


class PhotographerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photographer


class FloristSerializer(serializers.ModelSerializer):
    class Meta:
        model = Florist


class BeautySerializer(serializers.ModelSerializer):
    class Meta:
        model = Beauty


class StationerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Stationery


class CakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cake
