from django.core.files import File
from django.http import HttpResponse
from django.http.response import Http404
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from rest_framework import status
from rest_framework.response import Response

from ..core.views import GenericsListCreateAPIViewToken, APIViewToken
from ..vendors.models import Venue, VenueType, Reviews, Rooms, Place, Catering, Photographer, Florist, Cake, Stationery, \
    Beauty
from ..vendors.serializers import VenueSerializer, VenueTypeSerializer, ReviewsSerializer, RoomsSerializer, \
    PlaceSerializer, CateringSerializer, PhotographerSerializer, FloristSerializer, CakeSerializer, StationerySerializer, \
    BeautySerializer


class ExcelUpload(TemplateView):
    template_name = 'vendors/excel_upload.html'


excel_upload = ExcelUpload.as_view()


@csrf_exempt
def excel_post(request):
    # handle_uploaded_file(request.FILES['file'])

    from django.core.mail import send_mail
    send_mail(
        'Subject here',
        'Here is the message.',
        'no-reply@cubavuela.com',
        ['nerve2012@yandex.com'],
        fail_silently=False,
    )

    return HttpResponse("ok")


def handle_uploaded_file(f):
    f = File(f)

    from django.core.mail import send_mail
    send_mail(
        'Subject here',
        'Here is the message.',
        'no-reply@cubavuela.com',
        ['nerve2012@yandex.com'],
        fail_silently=False,
    )


class VenuesList(GenericsListCreateAPIViewToken):
    queryset = VenueType.objects.all()
    serializer_class = VenueSerializer


venues_list = VenuesList.as_view()


class VenuesDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        return Response(serializer.data)


venues_detail = VenuesDetail.as_view()


class VenuesUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


venues_update = VenuesUpdate.as_view()


class VenueDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


venues_delete = VenueDelete.as_view()


class VenueTypeDetail(APIViewToken):
    def get_object(self, pk):
        try:
            return VenueType.objects.get(pk=pk)
        except VenueType.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = VenueTypeSerializer(snippet)
        return Response(serializer.data)


venue_type_detail = VenueTypeDetail.as_view()


class VenueTypeList(GenericsListCreateAPIViewToken):
    queryset = VenueType.objects.all()
    serializer_class = VenueTypeSerializer


venue_type_list = VenueTypeList.as_view()


class VenueTypeUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return VenueType.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = VenueTypeSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


venue_type_update = VenueTypeUpdate.as_view()


class VenueTypeDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return VenueType.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


venue_type_delete = VenueTypeDelete.as_view()


class ReviewsListDetail(GenericsListCreateAPIViewToken):
    queryset = Reviews.objects.all()
    serializer_class = ReviewsSerializer


reviews_list = ReviewsListDetail.as_view()


class ReviewsDetail(APIViewToken):
    def get_object(self, pk):
        try:
            return Reviews.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ReviewsSerializer(snippet)
        return Response(serializer.data)


reviews_detail = ReviewsDetail.as_view()


class ReviewsUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Reviews.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ReviewsSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


reviews_update = ReviewsUpdate.as_view()


class ReviewsDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Reviews.objects.get(pk=pk)
        except Reviews.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


reviews_delete = ReviewsDelete.as_view()


# Rooms #

class RoomsList(GenericsListCreateAPIViewToken):
    queryset = Rooms.objects.all()
    serializer_class = RoomsSerializer


rooms_list = RoomsList.as_view()


class RoomsDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Rooms.objects.get(pk=pk)
        except Rooms.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = RoomsSerializer(snippet)
        return Response(serializer.data)


rooms_detail = RoomsDetail.as_view()


class RoomsUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Rooms.objects.get(pk=pk)
        except Rooms.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = RoomsSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


rooms_update = RoomsUpdate.as_view()


class RoomsDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Rooms.objects.get(pk=pk)
        except Rooms.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


rooms_delete = RoomsDelete.as_view()

# Venues Array #

class VenuesArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = VenueSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


venues_array_list = VenuesArrayList.as_view()


class VenuesArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


venues_array_detail = VenuesArrayDetail.as_view()


class VenuesArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = VenueSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


venues_array_delete = VenuesArrayDelete.as_view()


class VenuesArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Venue.objects.get(pk=pk)
        except Venue.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = VenueSerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = VenueSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


venues_array_update = VenuesArrayUpdate.as_view()


# Place #


class PlaceList(GenericsListCreateAPIViewToken):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer


place_list = PlaceList.as_view()


class PlaceDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Place.objects.get(pk=pk)
        except Place.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PlaceSerializer(snippet)
        return Response(serializer.data)


place_detail = PlaceDetail.as_view()


class PlaceUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Place.objects.get(pk=pk)
        except Place.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PlaceSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


place_update = PlaceUpdate.as_view()


class PlaceDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Place.objects.get(pk=pk)
        except Place.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


place_delete = PlaceDelete.as_view()


# Catering #


class CateringList(GenericsListCreateAPIViewToken):
    queryset = Catering.objects.all()
    serializer_class = CateringSerializer


catering_list = CateringList.as_view()


class CateringDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        return Response(serializer.data)


catering_detail = CateringDetail.as_view()


class CateringUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


catering_update = CateringUpdate.as_view()


class CateringDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


catering_delete = CateringDelete.as_view()


class CateringArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = CateringSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


catering_array_list = CateringArrayList.as_view()


class CateringArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


catering_array_detail = CateringArrayDetail.as_view()


class CateringArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = CateringSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


catering_array_delete = CateringArrayDelete.as_view()


class CateringArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Catering.objects.get(pk=pk)
        except Catering.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = CateringSerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = CateringSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


catering_array_update = CateringArrayUpdate.as_view()


# Photographer #


class PhotographerList(GenericsListCreateAPIViewToken):
    queryset = Photographer.objects.all()
    serializer_class = PhotographerSerializer


photographer_list = PhotographerList.as_view()


class PhotographerDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        return Response(serializer.data)


photographer_detail = PhotographerDetail.as_view()


class PhotographerUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


photographer_update = PhotographerUpdate.as_view()


class PhotographerDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


photographer_delete = PhotographerDelete.as_view()


class PhotographerArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = PhotographerSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


photographer_array_list = PhotographerArrayList.as_view()


class PhotographerArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


photographer_array_detail = PhotographerArrayDetail.as_view()


class PhotographerArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = PhotographerSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


photographer_array_delete = PhotographerArrayDelete.as_view()


class PhotographerArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Photographer.objects.get(pk=pk)
        except Photographer.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = PhotographerSerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = PhotographerSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


photographer_array_update = PhotographerArrayUpdate.as_view()


# Florist #


class FloristList(GenericsListCreateAPIViewToken):
    queryset = Florist.objects.all()
    serializer_class = FloristSerializer


florist_list = FloristList.as_view()


class FloristDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        return Response(serializer.data)


florist_detail = FloristDetail.as_view()


class FloristUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


florist_update = FloristUpdate.as_view()


class FloristDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


florist_delete = FloristDelete.as_view()


class FloristArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = FloristSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


florist_array_list = FloristArrayList.as_view()


class FloristArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


florist_array_detail = FloristArrayDetail.as_view()


class FloristArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = FloristSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


florist_array_delete = FloristArrayDelete.as_view()


class FloristArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Florist.objects.get(pk=pk)
        except Florist.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = FloristSerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = FloristSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


florist_array_update = FloristArrayUpdate.as_view()


# Cake #


class CakeList(GenericsListCreateAPIViewToken):
    queryset = Cake.objects.all()
    serializer_class = CakeSerializer


cake_list = CakeList.as_view()


class CakeDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        return Response(serializer.data)


cake_detail = CakeDetail.as_view()


class CakeUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


cake_update = CakeUpdate.as_view()


class CakeDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


cake_delete = CakeDelete.as_view()


class CakeArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = CakeSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


cake_array_list = CakeArrayList.as_view()


class CakeArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


cake_array_detail = CakeArrayDetail.as_view()


class CakeArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = CakeSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


cake_array_delete = CakeArrayDelete.as_view()


class CakeArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Cake.objects.get(pk=pk)
        except Cake.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = CakeSerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = CakeSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


cake_array_update = CakeArrayUpdate.as_view()


# Stationery #


class StationeryList(GenericsListCreateAPIViewToken):
    queryset = Stationery.objects.all()
    serializer_class = StationerySerializer


stationery_list = StationeryList.as_view()


class StationeryDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        return Response(serializer.data)


stationery_detail = StationeryDetail.as_view()


class StationeryUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


stationery_update = StationeryUpdate.as_view()


class StationeryDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


stationery_delete = StationeryDelete.as_view()


class StationeryArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = StationerySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


stationery_array_list = StationeryArrayList.as_view()


class StationeryArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


stationery_array_detail = StationeryArrayDetail.as_view()


class StationeryArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = StationerySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


stationery_array_delete = StationeryArrayDelete.as_view()


class StationeryArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Stationery.objects.get(pk=pk)
        except Stationery.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = StationerySerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = StationerySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


stationery_array_update = StationeryArrayUpdate.as_view()


# Beauty #


class BeautyList(GenericsListCreateAPIViewToken):
    queryset = Beauty.objects.all()
    serializer_class = BeautySerializer


beauty_list = BeautyList.as_view()


class BeautyDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        return Response(serializer.data)


beauty_detail = BeautyDetail.as_view()


class BeautyUpdate(APIViewToken):
    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


beauty_update = BeautyUpdate.as_view()


class BeautyDelete(APIViewToken):
    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def post(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


beauty_delete = BeautyDelete.as_view()


class BeautyArrayList(APIViewToken):

    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, format=None):
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        try:
            data = serializer.data[field]
            return Response(data)
        except KeyError:
            raise Http404

    def post(self, request, pk, field, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        data = serializer.data
        data[field].append(value)
        serializer = BeautySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


beauty_array_list = BeautyArrayList.as_view()


class BeautyArrayDetail(APIViewToken):

    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def get(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        try:
            return Response(serializer.data[field][int(index)])
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404


beauty_array_detail = BeautyArrayDetail.as_view()


class BeautyArrayDelete(APIViewToken):

    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        data = serializer.data
        try:
            del data[field][int(index)]
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = BeautySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


beauty_array_delete = BeautyArrayDelete.as_view()


class BeautyArrayUpdate(APIViewToken):

    def get_object(self, pk):
        try:
            return Beauty.objects.get(pk=pk)
        except Beauty.DoesNotExist:
            raise Http404

    def post(self, request, pk, field, index, format=None):
        value = request.data['value']
        snippet = self.get_object(pk)
        serializer = BeautySerializer(snippet)
        data = serializer.data
        try:
            data[field][int(index)] = value
        except KeyError:
            raise Http404
        except IndexError:
            raise Http404

        serializer = BeautySerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data[field])
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


beauty_array_update = BeautyArrayUpdate.as_view()
