# coding=utf-8
from django.conf.urls import url

from ..vendors.views import venues_list, excel_upload, excel_post, \
    venue_type_list, venue_type_detail, venue_type_update, venue_type_delete, reviews_list, \
    reviews_detail, reviews_update, reviews_delete, venues_detail, venues_update, venues_delete, rooms_list, \
    rooms_detail, rooms_update, rooms_delete, venues_array_list, venues_array_detail, venues_array_delete, \
    venues_array_update, place_list, place_detail, place_update, place_delete, catering_list, catering_detail, \
    catering_update, catering_delete, catering_array_list, catering_array_detail, catering_array_delete, \
    catering_array_update, photographer_list, photographer_detail, photographer_update, photographer_delete, \
    photographer_array_detail, photographer_array_list, photographer_array_delete, photographer_array_update, \
    florist_list, florist_detail, florist_update, florist_delete, florist_array_list, florist_array_detail, \
    florist_array_delete, florist_array_update, cake_list, cake_detail, cake_update, cake_delete, cake_array_list, \
    cake_array_detail, cake_array_delete, cake_array_update, stationery_list, stationery_detail, stationery_update, \
    stationery_delete, stationery_array_list, stationery_array_detail, stationery_array_delete, stationery_array_update, \
    beauty_list, beauty_detail, beauty_update, beauty_delete, beauty_array_list, beauty_array_detail, \
    beauty_array_delete, beauty_array_update

urlpatterns = [
    # VenueType
    url(r'^venues/types/$', venue_type_list),
    url(r'^venues/types/(?P<pk>[0-9]+)/$', venue_type_detail),
    url(r'^venues/types/(?P<pk>[0-9]+)/update/$', venue_type_update),
    url(r'^venues/types/(?P<pk>[0-9]+)/delete/$', venue_type_delete),

    # Reviews
    url(r'^reviews/$', reviews_list),
    url(r'^reviews/(?P<pk>[0-9]+)/$', reviews_detail),
    url(r'^reviews/(?P<pk>[0-9]+)/update/$', reviews_update),
    url(r'^reviews/(?P<pk>[0-9]+)/delete/$', reviews_delete),

    # Venues
    url(r'^venues/$', venues_list),
    url(r'^venues/(?P<pk>[0-9]+)/$', venues_detail),
    url(r'^venues/(?P<pk>[0-9]+)/update/$', venues_update),
    url(r'^venues/(?P<pk>[0-9]+)/delete/$', venues_delete),

    url(r'^venues/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', venues_array_list),
    url(r'^venues/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', venues_array_detail),
    url(r'^venues/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', venues_array_delete),
    url(r'^venues/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', venues_array_update),

    # Place
    url(r'^place/$', place_list),
    url(r'^place/(?P<pk>[0-9]+)/$', place_detail),
    url(r'^place/(?P<pk>[0-9]+)/update/$', place_update),
    url(r'^place/(?P<pk>[0-9]+)/delete/$', place_delete),

    # Rooms
    url(r'^rooms/$', rooms_list),
    url(r'^rooms/(?P<pk>[0-9]+)/$', rooms_detail),
    url(r'^rooms/(?P<pk>[0-9]+)/update/$', rooms_update),
    url(r'^rooms/(?P<pk>[0-9]+)/delete/$', rooms_delete),

    # Catering
    url(r'^catering/$', catering_list),
    url(r'^catering/(?P<pk>[0-9]+)/$', catering_detail),
    url(r'^catering/(?P<pk>[0-9]+)/update/$', catering_update),
    url(r'^catering/(?P<pk>[0-9]+)/delete/$', catering_delete),

    url(r'^catering/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', catering_array_list),
    url(r'^catering/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', catering_array_detail),
    url(r'^catering/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', catering_array_delete),
    url(r'^catering/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', catering_array_update),

    # Photographer
    url(r'^photographer/$', photographer_list),
    url(r'^photographer/(?P<pk>[0-9]+)/$', photographer_detail),
    url(r'^photographer/(?P<pk>[0-9]+)/update/$', photographer_update),
    url(r'^photographer/(?P<pk>[0-9]+)/delete/$', photographer_delete),

    url(r'^photographer/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', photographer_array_list),
    url(r'^photographer/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', photographer_array_detail),
    url(r'^photographer/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', photographer_array_delete),
    url(r'^photographer/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', photographer_array_update),

    # Florist
    url(r'^florist/$', florist_list),
    url(r'^florist/(?P<pk>[0-9]+)/$', florist_detail),
    url(r'^florist/(?P<pk>[0-9]+)/update/$', florist_update),
    url(r'^florist/(?P<pk>[0-9]+)/delete/$', florist_delete),

    url(r'^florist/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', florist_array_list),
    url(r'^florist/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', florist_array_detail),
    url(r'^florist/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', florist_array_delete),
    url(r'^florist/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', florist_array_update),

    # Cake
    url(r'^cake/$', cake_list),
    url(r'^cake/(?P<pk>[0-9]+)/$', cake_detail),
    url(r'^cake/(?P<pk>[0-9]+)/update/$', cake_update),
    url(r'^cake/(?P<pk>[0-9]+)/delete/$', cake_delete),

    url(r'^cake/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', cake_array_list),
    url(r'^cake/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', cake_array_detail),
    url(r'^cake/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', cake_array_delete),
    url(r'^cake/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', cake_array_update),

    # Stationery
    url(r'^stationery/$', stationery_list),
    url(r'^stationery/(?P<pk>[0-9]+)/$', stationery_detail),
    url(r'^stationery/(?P<pk>[0-9]+)/update/$', stationery_update),
    url(r'^stationery/(?P<pk>[0-9]+)/delete/$', stationery_delete),

    url(r'^stationery/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', stationery_array_list),
    url(r'^stationery/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', stationery_array_detail),
    url(r'^stationery/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', stationery_array_delete),
    url(r'^stationery/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', stationery_array_update),

    # Beauty
    url(r'^beauty/$', beauty_list),
    url(r'^beauty/(?P<pk>[0-9]+)/$', beauty_detail),
    url(r'^beauty/(?P<pk>[0-9]+)/update/$', beauty_update),
    url(r'^beauty/(?P<pk>[0-9]+)/delete/$', beauty_delete),

    url(r'^beauty/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/$', beauty_array_list),
    url(r'^beauty/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/$', beauty_array_detail),
    url(r'^beauty/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/delete/$', beauty_array_delete),
    url(r'^beauty/(?P<pk>[0-9]+)/(?P<field>[\w\-]+)/(?P<index>[0-9]+)/update/$', beauty_array_update),



]

