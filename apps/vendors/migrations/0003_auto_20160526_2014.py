# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-26 20:14
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('vendors', '0002_auto_20160331_0244'),
    ]

    operations = [
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('status', model_utils.fields.StatusField(default='active', max_length=100, no_check_for_status=True, verbose_name='estado')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, editable=False, monitor='status', verbose_name='status changed')),
                ('hidden', models.BooleanField(default=False, editable=False)),
                ('name', models.CharField(help_text='Nombre con que se identificar\xe1 en el sistema', max_length=200, verbose_name='nombre')),
                ('address', models.CharField(help_text='Address', max_length=250, verbose_name='Address')),
                ('phone', models.CharField(blank=True, help_text='Phone', max_length=15, null=True, verbose_name='Phone')),
                ('person', models.CharField(blank=True, help_text='Contact person', max_length=15, null=True, verbose_name='Contact person')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Email Address')),
                ('website', models.URLField(blank=True, help_text='Website', max_length=30, null=True)),
                ('max_capacity', models.PositiveIntegerField(blank=True, help_text='Maximum Capacity', null=True, validators=[django.core.validators.MinValueValidator(1)], verbose_name='Maximum Capacity')),
                ('additional_details_regarding_capacity', models.TextField(blank=True, help_text='Additional Details regarding Capacity', null=True, verbose_name='details')),
                ('on_site_bridal_suite', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='On Site Bridal Suite')),
                ('on_site_groom_suite', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='On Site Groom Suite')),
                ('handicap_access', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Handicap Access')),
                ('dj_facilities', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='DJ Facilities')),
                ('live_band_faciliites', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Live Band Faciliites')),
                ('in_house_wedding_coordinator', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='In House Wedding Coordinator')),
                ('curfew', models.TimeField(blank=True, null=True, verbose_name='Curfew')),
                ('curfew_extensions', models.CharField(help_text='Curfew Extensions', max_length=250, verbose_name='Curfew Extensions')),
                ('description', models.TextField(verbose_name='Description')),
                ('licenced_for_civil_ceremony', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Licenced for Civil Ceremony')),
                ('nearby_churches_and_distance', models.CharField(help_text='Curfew Extensions', max_length=250, verbose_name='Nearby Churches & Distance')),
                ('nearby_civil_ceremony_offices_and_distance', models.CharField(help_text='Curfew Extensions', max_length=250, verbose_name='Nearby civil ceremony offices & distance')),
                ('on_site_accomodation', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='On-Site Accomodation')),
                ('on_site_accomodation_capacity', models.PositiveIntegerField(blank=True, help_text='On Site Accomodation Capacity', null=True, verbose_name='On Site Accomodation Capacity')),
                ('additional_commentary_regarding_on_site_capacity', models.TextField(verbose_name='Additional Commentary')),
                ('on_site_catering', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='On-site Catering')),
                ('please_describe_your_catering_offer_including_pricing', models.TextField(verbose_name='Please describe')),
                ('is_third_party_catering_allowed', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Is third party catering allowed')),
                ('alcohol_license', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Alcohol License')),
                ('bring_your_own_corkage_option', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Bring Your Own/Corkage Option')),
                ('bring_your_own_corkage_pricing', models.CharField(help_text='Bring Your Own/Corkage Pricing', max_length=250, verbose_name='Bring Your Own/Corkage Pricing')),
                ('rentals_and_equipment_provided', models.IntegerField(blank=True, choices=[(1, 'YES'), (2, 'NO')], null=True, verbose_name='Rentals & Equipment Provided')),
                ('package_pricing', models.CharField(help_text='Package Pricing', max_length=250, verbose_name='Package Pricing')),
                ('image1', models.ImageField(upload_to=b'', verbose_name='Image #1')),
                ('image2', models.ImageField(upload_to=b'', verbose_name='Image #2')),
                ('image3', models.ImageField(upload_to=b'', verbose_name='Image #3')),
                ('image4', models.ImageField(upload_to=b'', verbose_name='Image #4')),
                ('image5', models.ImageField(upload_to=b'', verbose_name='Image #5')),
                ('reviews', models.TextField(blank=True, help_text='Reviews', null=True, verbose_name='Reviews')),
            ],
            options={
                'verbose_name': 'venue',
                'verbose_name_plural': 'venues',
            },
        ),
        migrations.CreateModel(
            name='VenueSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('status', model_utils.fields.StatusField(default='active', max_length=100, no_check_for_status=True, verbose_name='estado')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, editable=False, monitor='status', verbose_name='status changed')),
                ('hidden', models.BooleanField(default=False, editable=False)),
                ('name', models.CharField(help_text='Nombre con que se identificar\xe1 en el sistema', max_length=200, verbose_name='nombre')),
            ],
            options={
                'ordering': ('name',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VenueType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, unique=True, verbose_name='uuid')),
                ('status', model_utils.fields.StatusField(default='active', max_length=100, no_check_for_status=True, verbose_name='estado')),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, editable=False, monitor='status', verbose_name='status changed')),
                ('hidden', models.BooleanField(default=False, editable=False)),
                ('name', models.CharField(help_text='Nombre con que se identificar\xe1 en el sistema', max_length=200, verbose_name='nombre')),
            ],
            options={
                'ordering': ('name',),
                'abstract': False,
            },
        ),
        migrations.DeleteModel(
            name='Vendor',
        ),
        migrations.AddField(
            model_name='venue',
            name='venue_setting',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='type', to='vendors.VenueSetting', verbose_name='Venue Type'),
        ),
        migrations.AddField(
            model_name='venue',
            name='venue_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='type', to='vendors.VenueType', verbose_name='Venue Type'),
        ),
    ]
