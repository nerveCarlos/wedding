# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-21 17:01
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vendors', '0015_delete_chessboard'),
    ]

    operations = [
        migrations.AddField(
            model_name='venue',
            name='approved_vendor',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), blank=True, null=True, size=None),
        ),
    ]
