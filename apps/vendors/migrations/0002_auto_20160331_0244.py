# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-31 02:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vendors', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vendor',
            options={'verbose_name': 'vendor', 'verbose_name_plural': 'vendors'},
        ),
    ]
