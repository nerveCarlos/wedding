# coding=utf-8
from __future__ import unicode_literals

from django.utils.timezone import datetime
from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.contrib.postgres.fields import ArrayField
from ..core.models import BaseUUIDModel
from ..core.models import BaseNamedNomenclator

User = settings.AUTH_USER_MODEL


class Rooms(models.Model):
    maximum_capacity = models.PositiveIntegerField('Maximum Capacity', validators=[MinValueValidator(1)],
                                                   blank=True, null=True)
    date_start = models.DateTimeField(default=datetime.now, blank=True, null=True)
    date_end = models.DateTimeField(default=datetime.now, blank=True, null=True)
    id_venue = models.PositiveIntegerField('Venue', blank=True, null=True)
    price = models.CharField('Price', max_length=50)


class ImageModel(models.Model):
    image = models.ImageField(upload_to='images/%Y/%m/%d')
    date = models.DateTimeField(default=datetime.now)


class FileModel(models.Model):
    image = models.FileField(upload_to='files/%Y/%m/%d')
    date = models.DateTimeField(default=datetime.now)


class VenueType(BaseNamedNomenclator):
    short_description = models.CharField('Description', max_length=250, default='')


class Reviews(models.Model):
    created_on = models.DateTimeField(default=datetime.now)
    created_by = models.ForeignKey(User, related_name='reviews')
    comment = models.CharField('comment', max_length=250)
    vendor = models.ForeignKey('VendorBase', null=True, blank=True, related_name='reviews')


class ContactInformation(models.Model):
    name = models.CharField('Name', max_length=200, blank=True, null=True)
    address = models.CharField('Address', max_length=250, blank=True, null=True)
    phone = models.CharField('Phone', max_length=15, blank=True, null=True)
    person = models.CharField('Contact person', max_length=15, blank=True, null=True)
    email = models.EmailField('Email Address', blank=True)
    website = models.URLField(max_length=30, null=True, blank=True)

    class Meta:
        abstract = True
        ordering = ('name',)

    def __unicode__(self):
        return self.name

    def verbose_name(self):
        return unicode(self.__unicode__())


class VendorBase(BaseUUIDModel, ContactInformation):
    pass


class Venue(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Venue description
    short_description_of_your_venue = models.TextField('Description')
    venue_type = models.ForeignKey('VenueType', verbose_name="Venue Type", related_name='type', blank=True, null=True)
    maximum_capacity = models.PositiveIntegerField('Maximum Capacity', validators=[MinValueValidator(1)],
                                                   blank=True, null=True)
    additional_commentary_on_capacity = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    marquee_option = models.IntegerField('Marquee Option', choices=YESNO, blank=True, null=True)
    on_site_bridal_suite = models.IntegerField('On Site Bridal Suite', choices=YESNO, blank=True, null=True)
    on_site_groom_suite = models.IntegerField('On Site Groom Suite', choices=YESNO, blank=True, null=True)
    handicap_access_and_facilities = models.IntegerField('Handicap Access', choices=YESNO, blank=True, null=True)

    # Ceremony
    licensed_to_hold_a_civil_ceremony = models.IntegerField('Licenced for Civil Ceremony', choices=YESNO, blank=True,
                                                            null=True)
    nearby_churches_and_distance = ArrayField(
        ArrayField(
            models.CharField(max_length=50, blank=True),
            size=2,
            default=list,
        ),
        default=list,
    )
    nearby_civil_ceremony_offices_and_distance = ArrayField(
        ArrayField(
            models.CharField(max_length=50, blank=True),
            size=2,
            default=list,
        ),
        default=list,
    )

    # Overnight accommodation
    on_site_accomodation = models.IntegerField('On-Site Accomodation', choices=YESNO, blank=True, null=True)
    on_site_accomodation_capacity = models.PositiveIntegerField('On Site Accomodation Capacity',
                                                                blank=True, null=True)
    additional_commentary_regarding_on_site_capacity = ArrayField(
        models.CharField(max_length=250, blank=True),
        default=list,
    )

    # Catering and drinks
    on_site_catering = models.IntegerField('On-site Catering', choices=YESNO, blank=True, null=True)
    describe_your_catering_offer = models.TextField('Describe your catering offer')
    is_third_party_catering_allowed = models.IntegerField('Is third party catering allowed', choices=YESNO, blank=True,
                                                          null=True)
    alcohol_license = models.IntegerField('Alcohol License', choices=YESNO, blank=True,
                                          null=True)
    bring_your_own_corkage_option = models.IntegerField('Bring Your Own/Corkage Option', choices=YESNO, blank=True,
                                                        null=True)
    bring_your_own_corkage_pricing = models.CharField('Bring Your Own/Corkage Pricing', max_length=250)

    # Other
    in_house_wedding_coordinator = models.IntegerField('In House Wedding Coordinator', choices=YESNO, blank=True,
                                                       null=True)
    vendor_restrictions = models.IntegerField('Vendor Restrictions', choices=YESNO, blank=True, null=True)
    dj_facilities = models.IntegerField('DJ Facilities', choices=YESNO, blank=True, null=True)
    live_band_faciliites = models.IntegerField('Live Band Faciliites', choices=YESNO, blank=True, null=True)
    equipment_provided = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    curfew = models.TimeField('Curfew', blank=True, null=True)
    curfew_extensions = models.CharField('Curfew Extensions', max_length=250)
    any_other_restrictions = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    approved_vendors = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_vendors = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Pricing
    package_pricing = models.CharField('Package Pricing', max_length=250)
    description = models.CharField('Description', max_length=250, blank=True)

    class Meta:
        verbose_name = "Venue"
        verbose_name_plural = "Venues"


class Catering(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    awards_recognitions_distinguished_etc = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_or_approved_at = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    starting_price = models.CharField('Starting price', max_length=50)
    buffet_option = models.IntegerField('Buffet option', choices=YESNO, blank=True, null=True)
    cuisine = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    full_offer_description = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Alcohol
    alcohol = models.IntegerField('Acohol', choices=YESNO, blank=True, null=True)
    corkage_option = models.IntegerField('Corkage option', choices=YESNO, blank=True, null=True)
    description = models.CharField('Description', max_length=250)
    offer = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Area coverage
    area_coverage = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Any other commentary
    rentals = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    tastings_option = models.IntegerField('Tastings option', choices=YESNO, blank=True, null=True)
    photos_of_sample_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    class Meta:
        verbose_name = "Catering"
        verbose_name_plural = "Caterings"


class Photographer(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    style = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    shooting_mode = models.TextField('Shooting mode', blank=True, null=True)
    awards_recognitions_distinguished_etc = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_or_approved_at = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    standard_package_price = models.CharField('Standard package price', max_length=100)
    full_offer_description = models.CharField('Full offer description', max_length=150)

    # Area coverage
    area_coverage = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    accommodation_and_travel = models.CharField('Accomodation & Travel ', max_length=200)

    # Any other commentary
    photos_of_sample_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    class Meta:
        verbose_name = "Photographer"
        verbose_name_plural = "Photographers"


class Florist(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    awards_publications_similar = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_or_approved_at = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    standard_package_price = models.CharField('Standard package price', max_length=100)
    full_offer_description = models.CharField('Full offer description', max_length=150)

    # Area coverage
    area_coverage = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Any other commentary
    photos_of_sample_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    class Meta:
        verbose_name = "Florist"
        verbose_name_plural = "Florists"


class Cake(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    awards_publications_similar = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_or_approved_at = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    standard_cake_price = models.CharField('Standard cake price', max_length=100)
    flavours = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    types_of_cakes = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    other_desserts_available = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    full_offer_description = models.CharField('Full offer description', max_length=150)
    tastings = models.IntegerField('Tastings', choices=YESNO, blank=True, null=True)
    delivery_to_the_venue = models.IntegerField('Delivery to the venue', choices=YESNO, blank=True, null=True)

    # Area coverage
    area_coverage = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Any other commentary
    photos_of_sample_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    class Meta:
        verbose_name = "Cake"
        verbose_name_plural = "Cake"


class Stationery(VendorBase):
    YES = 1
    NO = 2
    OTHER = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    YESNODIGITAL = (
        (YES, 'YES'),
        (NO, 'NO'),
        (OTHER, 'Digital desig only'),
    )
    YESNOPRESET = (
        (YES, 'YES'),
        (NO, 'NO'),
        (OTHER, 'Just pre-set designs'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    awards_publications_similar = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    recommended_or_approved_at = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    standard_package_price = models.CharField('Standard package price', max_length=250)
    list_of_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )
    print_offer = models.IntegerField('Delivery to the venue', choices=YESNODIGITAL, blank=True, null=True)
    calligraphy = models.IntegerField('Delivery to the venue', choices=YESNO, blank=True, null=True)
    bespoke_design = models.IntegerField('Delivery to the venue', choices=YESNOPRESET, blank=True, null=True)
    full_offer_description = models.CharField('Full offer description', max_length=150)
    free_samples = models.IntegerField('Free samples', choices=YESNO, blank=True, null=True)

    # Any other commentary
    photos_of_sample_products = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    class Meta:
        verbose_name = "Stationery"
        verbose_name_plural = "Stationery"


class Beauty(VendorBase):
    YES = 1
    NO = 2
    YESNO = (
        (YES, 'YES'),
        (NO, 'NO'),
    )
    # Business description
    short_description_of_your_business = models.TextField('Description')
    awards_publications_similar = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Offer and pricing
    standard_package_price = models.CharField('Standard package price', max_length=200)
    key_services = models.CharField('Key services', max_length=20)
    full_offer_description = models.CharField('Full offer description', max_length=150)
    pre_wedding_trial = models.CharField('Pre-wedding trial', max_length=250)

    # Area coverage
    area_coverage = ArrayField(
        models.CharField(max_length=200),
        blank=True,
        default=list,
    )

    # Any other commentary
    photos_of_sample_products = models.TextField('Photos of sample products', max_length=500)

    class Meta:
        verbose_name = "Beauty"
        verbose_name_plural = "Beauty"


class Place(models.Model):
    latitude = models.FloatField("latitude")
    longitude = models.FloatField("longitude")
    localizedDescription = models.CharField("localized description", max_length=150)
    postalCode = models.CharField("postal code", max_length=6)
    city = models.CharField("city", max_length=50)
    country = models.CharField("country", max_length=50)
