# coding=utf-8
from django.conf.urls import url, include

from ..accounts import views as accounts

urlpatterns = [
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r"^signup/$", accounts.signup, name="account_signup"),
]
