from django.contrib.auth.models import User
from rest_framework import generics

from ..accounts.permissions import IsAuthenticatedOrCreate
from ..accounts.serializers import UserSerializer


class SignUp(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedOrCreate,)


signup = SignUp.as_view()
