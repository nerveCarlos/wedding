# Installation #
The python version used is **2.7**.

After cloning the repo to your local computer run on the command line

```
#!bash

virtualenv venv
```
This will create a virtual environment to install the python packages locally. 
Then run

```
#!python

source venv/bin/activate
```

followed by



```
#!python

pip install -r requirements.txt 
```
This assumes that the file requirements.txt is located at the root of the project source code and you are at that directory.
After running this command all the required packages will be installed properly.
For this project we are using a Postgres SQL database. So from now on we assume you have one Postgres server you can connect to.
The following step consist on editing the file *wedding/settings/local.py* with the apropiate values to connect to that server.

After sucessfully setting the database connection properties you should run
```
#!python

python manage.py migrate
```
to create the database tables that maps to the django models.
Once this command completes successfully you can run the app using


```
#!python

python manage.py runserver
```

# Admin section #
If you to the admin section of the site you will be able to look at the different database models we have created so far and play with the different fields. 
The url is (assuming the server is running at **http://127.0.0.1:8000/**)

[http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin) and you can login there. If you are wondering which credentials shall you use for login. The answer is that you can create a new superuser using the command

```
#!python

python manage.py createsuperuser
```

### What is this repository for? ###

This is the source code of the website MWS

Copyright Sukkyservices @ 2016

Version 0.0.1
 [Website](https://bitbucket.org/tutorials/markdowndemo)

## Bugs ##
If you found any bug in this readme please contact me at [miguel@wilderness.london](mailto:miguel@wilderness.london)