"""wedding URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.views import static
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django_xmlrpc import views
from apps.core.views import HomePageView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^weblog/', include('apps.blog.urls', namespace='blog')),
    url(r'^weblog/', include('zinnia.urls', namespace='zinnia')),
    url(r'^comments/', include('django_comments.urls')),
    url(r'^xmlrpc/$', views.handle_xmlrpc),
    url(r'^accounts/', include('apps.accounts.urls')),
    url(r'^vendors/', include('apps.vendors.urls')),
    url(r'^payments/', include('apps.payments.urls')),
    url(r'^docs/', include('rest_framework_docs.urls')),
    url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT})
]
