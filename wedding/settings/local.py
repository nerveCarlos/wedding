# coding=utf-8
"""
This is an example settings/local.py file.
These settings overrides what's in settings/base.py
"""

from . import base

TEMPLATE_DEBUG = DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'da1q3tcj3mjo9v',                      # Or path to database file if using sqlite3.
        'USER': 'uvzvgvfzpvngbu',                      # Not used with sqlite3.
        'PASSWORD': 'MbKrKOHAw8gRtzjabatzsO5_Lq',                  # Not used with sqlite3.
        'HOST': 'ec2-54-235-254-199.compute-1.amazonaws.com',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Recipients of traceback emails and other notifications.
ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
MANAGERS = ADMINS

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []
INTERNAL_IPS = ('127.0.0.1')

# SECURITY WARNING: keep the secret key used in production secret!
# Hardcoded values can leak through source control. Consider loading
# the secret key from an environment variable or a file instead.
SECRET_KEY = 'esw=sfot&amp;_!&amp;w7__sif)3tklk3)lhczd4s+l68o3)sw7f!eg51'
